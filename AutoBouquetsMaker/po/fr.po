msgid ""
msgstr ""
"Project-Id-Version: enigma2-oe-alliance-plugins 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-10-29 20:53+0100\n"
"PO-Revision-Date: 2018-07-31 12:31+0200\n"
"Last-Translator: neoatomic <neoatomic@users.noreply.github.com>\n"
"Language-Team: french-EGAMI\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Poedit 2.1.1\n"
"X-Poedit-SourceCharset: iso-8859-15\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../src/scanner/main.py:761
msgid "%a %e %b  %-H:%M"
msgstr "%a %e %b  %-H:%M"

#: ../src/scanner/main.py:709
#, python-format
msgid "%s scheduled update"
msgstr ""

#: ../src/scanner/main.py:707
#, fuzzy, python-format
msgid ""
"%s update is about to start.\n"
"Do you want to allow this?"
msgstr ""
"Vos bouquets sont sur le point d'être mis à jour,\n"
"Voulez-vous permettre cela ?"

#: ../src/plugin.py:51 ../src/plugin.py:67
msgid "---- ABM - 'text' ----"
msgstr "---- ABM -'texte' ----"

#: ../src/plugin.py:49 ../src/plugin.py:65
msgid "-------- text --------"
msgstr "-------- text --------"

#: ../src/plugin.py:44 ../src/plugin.py:60
msgid "< - - text - - >"
msgstr "< - - texte - - >"

#: ../src/plugin.py:43 ../src/plugin.py:59
msgid "<-- text -->"
msgstr "<-- texte -->"

#: ../src/plugin.py:48 ../src/plugin.py:64
msgid "= = = text = = ="
msgstr "= = = texte = = ="

#: ../src/plugin.py:46 ../src/plugin.py:62
msgid "= = text = ="
msgstr "= = texte = ="

#: ../src/plugin.py:50 ../src/plugin.py:66
msgid "== ABM - 'text' =="
msgstr "== ABM - 'texte' =="

#: ../src/plugin.py:45 ../src/plugin.py:61
msgid "== text =="
msgstr "== texte =="

#: ../src/plugin.py:47 ../src/plugin.py:63
msgid "=== text ==="
msgstr "=== texte ==="

#: ../src/scanner/frequencyfinder.py:142 ../src/scanner/frequencyfinder.py:147
msgid "ABM frequency finder"
msgstr ""

#: ../src/menu.py:247
msgid "ABM log file has been saved to the tmp directory"
msgstr "Le fichier journal ABM a été enregistré dans le répertoire tmp"

#: ../src/about.py:24 ../src/menu.py:91
msgid "About"
msgstr "À propos"

#: ../src/setup.py:503
msgid "Add provider markers"
msgstr "Ajouter des marqueurs de fournisseur"

#: ../src/setup.py:502
#, fuzzy
msgid "Add provider name to bouquets"
msgstr "Ajouter le préfix de l'opérateur aux bouquets"

#: ../src/scanner/bouquetswriter.py:621
msgid "All channels"
msgstr "Toutes les chaînes"

#: ../src/setup.py:493
msgid "Allows you to set a schedule to perform a scan "
msgstr "Vous permet de programmer un scan"

#: ../src/deletebouquets.py:38
msgid "Are you sure you want to remove all bouquets created by ABM?"
msgstr "Êtes-vous sûr de vouloir supprimer tous les bouquets créés par ABM ?"

#: ../src/setup.py:595
msgid "At least one day of the week must be selected"
msgstr ""

#: ../src/about.py:24 ../src/menu.py:44 ../src/plugin.py:96
#: ../src/plugin.py:110 ../src/scanner/main.py:44 ../src/scanner/main.py:104
#: ../src/setup.py:574
msgid "AutoBouquetsMaker"
msgstr "AutoBouquetsMaker"

#: ../src/setup.py:446
msgid "AutoBouquetsMaker Configure"
msgstr "AutoBouquetsMaker - Configurer"

#: ../src/hidesections.py:26
msgid "AutoBouquetsMaker Hide sections"
msgstr "AutoBouquetsMaker - Cacher sections"

#: ../src/keepbouquets.py:27
msgid "AutoBouquetsMaker Keep bouquets"
msgstr "AutoBouquetsMaker - Conserver bouquets"

#: ../src/menu.py:228
msgid "AutoBouquetsMaker Log"
msgstr "AutoBouquetsMaker - Log"

#: ../src/setup.py:49
msgid "AutoBouquetsMaker Providers"
msgstr "AutoBouquetsMaker - Opérateurs"

#: ../src/plugin.py:112
msgid "AutoBouquetsMaker Scanner"
msgstr "AutoBouquetsMaker Scanner"

#: ../src/scanner/main.py:191
msgid "Bouquets generation..."
msgstr "Génération de bouquets......"

#: ../src/hidesections.py:31 ../src/keepbouquets.py:32 ../src/ordering.py:36
#: ../src/setup.py:68 ../src/setup.py:465 ../src/setup.py:582
msgid "Cancel"
msgstr "Abandon"

#: ../src/scanner/frequencyfinder.py:347 ../src/scanner/main.py:363
#, fuzzy
msgid "Cannot allocate the demuxer."
msgstr "Impossible d'allouer le demuxer"

#: ../src/scanner/frequencyfinder.py:322 ../src/scanner/main.py:334
msgid "Cannot free NIM because a recording is in progress"
msgstr "Impossible de libérer NIM car un enregistrement est en cours."

#: ../src/scanner/frequencyfinder.py:336 ../src/scanner/main.py:357
msgid "Cannot get frontend"
msgstr "Impossible d'obtenir le frontend"

#: ../src/scanner/frequencyfinder.py:326 ../src/scanner/main.py:338
msgid "Cannot get the NIM"
msgstr "Impossible d'obtenir le NIM"

#: ../src/scanner/main.py:489
msgid "Cannot read data"
msgstr "Impossible de lire les données."

#: ../src/updateproviders.py:167
msgid "Cannot read version"
msgstr "Impossible de lire la version"

#: ../src/scanner/frequencyfinder.py:263 ../src/scanner/main.py:279
msgid "Cannot retrieve Resource Manager instance"
msgstr "Impossible de récupérer l'instance Resource Manager"

#: ../src/updateproviders.py:118
msgid "Checking version compatibility..."
msgstr "Vérification de la compatibilité des versions....."

#: ../src/setup.py:505
msgid "Choose the style of markers that separate one provider from another in bouquet indexes."
msgstr "Choisissez le style de marqueurs qui séparent un fournisseur d'un autre dans les index de bouquet."

#: ../src/setup.py:506
msgid "Choose the style of the markers that separate channels into groups in the channel lists."
msgstr "Choisissez le style des marqueurs qui séparent les canaux en groupes dans les listes de canaux."

#: ../src/about.py:36 ../src/menu.py:241 ../src/updateproviders.py:287
msgid "Close"
msgstr "Fermer"

#: ../src/updateproviders.py:230
#, python-format
msgid "Config file for %s didn't need updating."
msgstr "Le fichier de configuration pour %s n'avait pas besoin d'être mis à jour."

#: ../src/updateproviders.py:228
#, fuzzy, python-format
msgid "Config file for %s does not need updating."
msgstr "Le fichier de configuration pour %s n'a pas besoin d'être mis à jour."

#: ../src/updateproviders.py:249
#, fuzzy, python-format
msgid "Config file for %s does not parse."
msgstr "Le fichier de configuration pour %s ne contient pas d'analyse."

#: ../src/updateproviders.py:240 ../src/updateproviders.py:242
#, python-format
msgid "Config file for %s has been updated."
msgstr "Le fichier de configuration pour %s a été mis à jour."

#: ../src/updateproviders.py:259
#, fuzzy, python-format
msgid "Config file for %s is faulty."
msgstr "Le fichier de configuration pour %s est défectueux."

#: ../src/menu.py:77
msgid "Configure"
msgstr "Configurer"

#: ../src/setup.py:320
#, fuzzy
msgid "Create FTA HD bouquet"
msgstr "générer un bouquet HD en clair"

#: ../src/setup.py:317
#, fuzzy
msgid "Create FTA bouquet"
msgstr "générer le bouquet FTA (en clair)"

#: ../src/setup.py:314
#, fuzzy
msgid "Create HD bouquet"
msgstr "générer le bouqet HD"

#: ../src/setup.py:305
#, fuzzy
msgid "Create main bouquet"
msgstr "générer le bouquet principal"

#: ../src/setup.py:311
#, fuzzy
msgid "Create sections bouquets"
msgstr "générer les bouquets sections"

#: ../src/setup.py:308
#, fuzzy
msgid "Custom bouquet for main"
msgstr "bouquet personnalisé comme principal"

#: ../src/menu.py:89
msgid "DVB-T frequency finder"
msgstr ""

#: ../src/menu.py:86
#, fuzzy
msgid "Delete ABM bouquets"
msgstr "Supprimer les bouquets"

#: ../src/scanner/main.py:508
msgid "Done"
msgstr "Fini"

#: ../src/updateproviders.py:179
msgid "Downloading latest file from Github..."
msgstr "Télécharger le dernier fichier de Github....."

#: ../src/scanner/main.py:726
msgid "Enough Retries, delaying till next schedule."
msgstr "Assez d'essais, on reporte jusqu’à la prochaine programmation."

#: ../src/menu.py:68
msgid "Exit"
msgstr "Sortir"

#: ../src/setup.py:510
msgid "Extra debug"
msgstr "Débogage supplémentaire"

#: ../src/scanner/bouquetswriter.py:885
msgid "FTA Channels"
msgstr "Chaînes en clair"

#: ../src/scanner/bouquetswriter.py:669 ../src/scanner/bouquetswriter.py:842
msgid "FTA HD Channels"
msgstr "Chaînes en clair HD"

#: ../src/setup.py:302
msgid "FTA only"
msgstr "Seulement chaîne en clair"

#: ../src/updateproviders.py:194
#, python-format
msgid "Failed to retrieve file for %s. Error: %s %s"
msgstr "Impossible de retrouver le fichier pour %s. Erreur: %s %s"

#: ../src/updateproviders.py:141
#, python-format
msgid "Failed to retrieve version file. Error: %s %s"
msgstr "Échec de récupération du fichier de version. Erreur : %s %s"

#: ../src/updateproviders.py:195
msgid "Failed..."
msgstr "Échec..."

#: ../src/updateproviders.py:52
msgid "Finding configured providers..."
msgstr "Trouver des fournisseurs configurés..."

#: ../src/scanner/frequencyfinder.py:186
#, python-format
msgid "Found %d unique transponder"
msgid_plural "Found %d unique transponders"
msgstr[0] ""
msgstr[1] ""

#: ../src/scanner/frequencyfinder.py:79
msgid "FrequencyFinder"
msgstr ""

#: ../src/setup.py:578
msgid "Friday"
msgstr ""

#: ../src/scanner/bouquetswriter.py:666 ../src/scanner/bouquetswriter.py:799
msgid "HD Channels"
msgstr "Chaînes HD"

#: ../src/menu.py:82
msgid "Hide sections"
msgstr "Cacher sections"

#: ../src/setup.py:508
msgid "If a service is carried on a satellite that is not configured, 'yes' means the channel will not appear in the channel list, 'no' means the channel will show in the channel list but be greyed out and not be accessible."
msgstr "Si un service est affecté à un satellite qui n'est pas configuré, «oui» signifie que la chaîne n'apparaîtra pas dans la liste des chaînes, «non» signifie que la chaînes'affichera dans la liste des chaînes, mais grisée et inaccessible."

#: ../src/setup.py:497
msgid "If the receiver is in 'Deep Standby' when the schedule is due, wake it up to perform a scan."
msgstr ""

#: ../src/setup.py:499
msgid "If the receiver was woken from 'Deep Standby' and is currently in 'Standby' and no recordings are in progress return it to 'Deep Standby' once the scan has completed."
msgstr ""

#: ../src/setup.py:509
msgid "Include 'not indexed' channels"
msgstr "Inclure les chaînes 'non indexées'"

#: ../src/updateproviders.py:161
#, python-format
msgid "Incompatible versions: %s > %s"
msgstr "Versions incompatibles : %s > %s"

#: ../src/setup.py:501
msgid "Keep all non-ABM bouquets"
msgstr "Garder tous bouquets non-ABM"

#: ../src/menu.py:84
msgid "Keep bouquets"
msgstr "Garder bouquets"

#: ../src/scanner/bouquetswriter.py:542
msgid "Last Scanned"
msgstr "Dernière analyse"

#: ../src/scanner/main.py:90
msgid "Loading bouquets..."
msgstr "Chargement des bouquets....."

#: ../src/setup.py:578
msgid "Monday"
msgstr ""

#: ../src/ordering.py:73 ../src/ordering.py:79 ../src/ordering.py:108
#: ../src/ordering.py:114
msgid "Move down"
msgstr "Vers le bas"

#: ../src/ordering.py:75 ../src/ordering.py:78 ../src/ordering.py:110
#: ../src/ordering.py:113
msgid "Move up"
msgstr "Vers le haut"

#: ../src/updateproviders.py:148 ../src/updateproviders.py:206
#, fuzzy
msgid "Network connection error."
msgstr "Erreur de connexion au réseau"

#: ../src/updateproviders.py:145 ../src/updateproviders.py:203
#, python-format
msgid ""
"Network connection error: \n"
"%s"
msgstr ""
"Erreur de connexion réseau :\n"
"%s"

#: ../src/scanner/main.py:273
msgid "No NIMs found for "
msgstr "Aucun NIM n'a été trouvé pour "

#: ../src/scanner/main.py:230
msgid "No area found"
msgstr "Aucune zone trouvée"

#: ../src/scanner/frequencyfinder.py:172
msgid "No frequencies to search"
msgstr ""

#: ../src/updateproviders.py:131
msgid "No providers are configured."
msgstr "Aucun fournisseur n'est configuré."

#: ../src/updateproviders.py:97
msgid "No providers configured."
msgstr "Aucun fournisseur n'est configuré."

#: ../src/scanner/frequencyfinder.py:216
msgid "No terrestrial multiplexes found."
msgstr ""

#: ../src/scanner/frequencyfinder.py:239 ../src/scanner/frequencyfinder.py:257
#, fuzzy
msgid "No terrestrial tuner found."
msgstr "Aucune zone trouvée"

#: ../src/scanner/frequencyfinder.py:304
#, fuzzy
msgid "No valid NIM found for terrestrial."
msgstr "Aucun NIM valide n'a été trouvé pour "

#: ../src/scanner/frequencyfinder.py:213
msgid "Only DVB-T2 multiplexes found. Insufficient data to create a provider file."
msgstr ""

#: ../src/setup.py:507
msgid "Place bouquets at"
msgstr "Placer bouquets vers"

#: ../src/scanner/main.py:96
msgid "Please first setup, in configuration"
msgstr "Veuillez procéder à la configuration d'abord"

#: ../src/ordering.py:59 ../src/setup.py:77 ../src/setup.py:474
msgid "Please wait..."
msgstr "Veuillez patientez..."

#: ../src/plugin.py:28
msgid "Press OK"
msgstr ""

#: ../src/setup.py:496
#, fuzzy
msgid "Press OK to select which days to perform a scan."
msgstr "Vous permet de programmer un scan"

#: ../src/menu.py:78
msgid "Providers"
msgstr "Opérateurs"

#: ../src/menu.py:80 ../src/ordering.py:25
msgid "Providers order"
msgstr "Classement opérateurs"

#: ../src/scanner/bouquetswriter.py:929 ../src/scanner/bouquetswriter.py:930
msgid "Radio Channels"
msgstr "Chaînes radio"

#: ../src/setup.py:596
msgid "Radio Times Emulator"
msgstr ""

#: ../src/scanner/frequencyfinder.py:365
#, python-format
msgid "Reading %s MHz (ch %s)"
msgstr ""

#: ../src/scanner/main.py:446
#, python-format
msgid "Reading %s..."
msgstr "Lecture %s..."

#: ../src/hidesections.py:104 ../src/keepbouquets.py:104 ../src/ordering.py:153
#: ../src/setup.py:435 ../src/setup.py:565 ../src/setup.py:604
msgid "Really close without saving settings?"
msgstr "Vraiment quitter sans sauvegarder les paramètres?"

#: ../src/setup.py:297
msgid "Region"
msgstr ""

#: ../src/updateproviders.py:227 ../src/updateproviders.py:239
#: ../src/updateproviders.py:248
#, python-format
msgid "Retrieved config file for %s"
msgstr "Récupéré le fichier de configuration pour %s"

#: ../src/updateproviders.py:251
#, python-format
msgid "Retrieved config file for %s does not parse."
msgstr "Le fichier de configuration récupéré pour %s n'analyse pas."

#: ../src/updateproviders.py:261
#, python-format
msgid "Retrieved config file for %s is faulty or incomplete."
msgstr "Le fichier de configuration récupéré pour %s est défectueux ou incomplet."

#: ../src/updateproviders.py:258
#, python-format
msgid "Retrieved provider file for %s"
msgstr "Fichier du fournisseur récupéré pour %s"

#: ../src/updateproviders.py:178
#, python-format
msgid "Retrieving config file for %s"
msgstr "Récupération du fichier de configuration pour %s"

#: ../src/setup.py:578
msgid "Saturday"
msgstr ""

#: ../src/hidesections.py:32 ../src/setup.py:69 ../src/setup.py:466
#: ../src/setup.py:583
msgid "Save"
msgstr "Sauver"

#: ../src/menu.py:240
msgid "Save Log"
msgstr "Enregistrer le journal"

#: ../src/scanner/frequencyfinder.py:200
#, fuzzy
msgid "Saving data"
msgstr "Lancer la mise à jour....."

#: ../src/menu.py:69
msgid "Scan"
msgstr "Analyser"

#: ../src/scanner/frequencyfinder.py:161
msgid "Scanning for active transponders"
msgstr ""

#: ../src/setup.py:496
msgid "Schedule days of the week"
msgstr ""

#: ../src/setup.py:499
msgid "Schedule return to deep standby"
msgstr ""

#: ../src/setup.py:493
msgid "Schedule scan"
msgstr "Programmer un scan"

#: ../src/setup.py:495
#, fuzzy
msgid "Schedule time of day"
msgstr "Programmer un scan"

#: ../src/setup.py:497
msgid "Schedule wake from deep standby"
msgstr ""

#: ../src/setup.py:511
msgid "Select \"yes\" to show the \"DVB-T frequency finder\" tool in the main menu. This tool is used to create a working provider file for difficult areas of the UK, e.g. areas covered by repeaters, etc."
msgstr ""

#: ../src/setup.py:574 ../src/setup.py:596
msgid "Select days"
msgstr ""

#: ../src/setup.py:308
msgid "Select your own bouquet from the list, please note that the only the first 100 channels for this bouquet will be used."
msgstr "Choisir votre propre bouquet dans la liste, notez que seul les 100 premières chaînes de ce bouquet seront utilisées."

#: ../src/scanner/frequencyfinder.py:252
#, fuzzy
msgid "Selected tuner is not configured."
msgstr "Sauter services des satellites non configurés"

#: ../src/scanner/main.py:192 ../src/scanner/main.py:207
#: ../src/scanner/main.py:447 ../src/scanner/main.py:509
#, python-format
msgid "Services: %d video - %d radio"
msgstr "Services : %d vidéo - %d radio"

#: ../src/scanner/main.py:91
msgid "Services: 0 video - 0 radio"
msgstr "Services : 0 vidéo - 0 radio"

#: ../src/setup.py:495
#, fuzzy
msgid "Set the time of day to perform a scan."
msgstr "Définir l'heure de la journée pour effectuer le scan."

#: ../src/setup.py:511
msgid "Show DVB-T frequency finder"
msgstr ""

#: ../src/setup.py:512
msgid "Show in extensions"
msgstr "Afficher dans les extensions"

#: ../src/menu.py:90
msgid "Show log"
msgstr "Afficher log"

#: ../src/setup.py:508
msgid "Skip services on not configured sats"
msgstr "Sauter services des satellites non configurés"

#: ../src/menu.py:85
msgid "Start scan"
msgstr "Démarrer scan"

#: ../src/scanner/frequencyfinder.py:86 ../src/scanner/main.py:58
msgid "Starting scanner"
msgstr "Démarrage du scanner"

#: ../src/scanner/frequencyfinder.py:160
#, fuzzy
msgid "Starting search..."
msgstr "Lancer la mise à jour....."

#: ../src/updateproviders.py:117
msgid "Starting update..."
msgstr "Lancer la mise à jour....."

#: ../src/setup.py:506
msgid "Style of bouquet marker"
msgstr "Style du marqueur de bouquet"

#: ../src/setup.py:505
msgid "Style of provider marker"
msgstr "Style du marqueur du fournisseur"

#: ../src/setup.py:578
msgid "Sunday"
msgstr ""

#: ../src/setup.py:323
#, fuzzy
msgid "Swap channels"
msgstr "permuter les chaînes"

#: ../src/setup.py:302
msgid "This affects all bouquets. Select 'no' to scan in all services. Select 'yes' to skip encrypted ones."
msgstr "Ceci affecte tous les bouquets. Sélectionnez 'non' pour balayer tous les services. Sélectionnez 'oui' pour sauter ceux qui sont cryptés."

#: ../src/setup.py:510
msgid "This feature is for development only. Requires debug logs to be enabled or enigma2 to be started in console mode."
msgstr "Cette fonctionnalité est réservée au développement. Nécessite que les journaux de débogage soient activés ou qu'enigma2 soit démarré en mode console."

#: ../src/setup.py:297
msgid "This option allows you to choose what region of the country you live in, so it populates the correct channels for your region."
msgstr "Cette option vous permets de définir la région du pays dans lequel vous vivez, et donc les chaînes régionales seront correctement provisionnées."

#: ../src/setup.py:294
msgid "This option enables the current selected provider."
msgstr "Cette options actives l'opérateur actuellement sélectionné."

#: ../src/setup.py:305
msgid "This option has several choices \"Yes\", (create a bouquet with all the channels in it), \"Yes HD only\", (will group all HD channels into this bouquet), \"Custom\", (allows you to select your own bouquet), \"No\", (do not use a main bouquet)"
msgstr "Cette option a plusieurs choix \"Oui\", (créer un bouquet contenant toutes les chaînes), \"Oui seulement HD \", (regroupera toutes les chaînes HD de ce bouquet), \"Personnalisé\", (vous permet de choisir votre propre bouquet), \"Non\", (ne pas utiliser un bouquet principal)"

#: ../src/setup.py:503
msgid "This option places markers in the bouquet index to group all bouquets of each provider."
msgstr "Cette option place des marqueurs dans l'index des bouquets pour regrouper tous les bouquets de chaque fournisseur."

#: ../src/setup.py:502
#, fuzzy
msgid "This option will add the provider's name to bouquet names."
msgstr "Cette option passera le nom opérateur en préfixe au nom du bouquet."

#: ../src/setup.py:507
msgid "This option will allow you choose where to place the created bouquets."
msgstr "Cette option vous permettra de choisir où placer les bouquets créés."

#: ../src/setup.py:320
msgid "This option will create a FreeToAir High Definition bouquet, it will group all FTA HD channels into this bouquet."
msgstr "Cette option créera un bouquet chaîne en clair Haute Définition, il regroupera toutes les chaînes HD en clair dans ce bouquet."

#: ../src/setup.py:317
msgid "This option will create a FreeToAir bouquet, it will group all free channels into this bouquet."
msgstr "Cette option créera un bouquet chaîne en clair, elle regroupera tous les canaux gratuits dans ce bouquet."

#: ../src/setup.py:314
msgid "This option will create a High Definition bouquet, it will group all HD channels into this bouquet."
msgstr "Cette option va créer un bouquet Haute Définition, il va regrouper toutes les chaînes HD dans ce bouquet."

#: ../src/setup.py:311
msgid "This option will create bouquets for each type of channel, ie Entertainment, Movies, Documentary."
msgstr "Cette option va créer des bouquets pour chaque type de chaîne, ex Loisirs, Films, Documentaire."

#: ../src/setup.py:323
msgid "This option will swap SD versions of channels with HD versions. (eg BBC One SD with BBC One HD, Channel Four SD with with Channel Four HD)"
msgstr "Cette option permet de permuter les versions SD des chaînes avec les versions HD. (par exemple BBC One SD avec BBC One HD, Channel Four SD avec Channel Four HD)"

#: ../src/setup.py:578
msgid "Thursday"
msgstr ""

#: ../src/setup.py:578
msgid "Tuesday"
msgstr ""

#: ../src/scanner/frequencyfinder.py:185
#, python-format
msgid "Tuning %s MHz (ch %s)"
msgstr ""

#: ../src/scanner/main.py:206
#, python-format
msgid "Tuning %s..."
msgstr "Syntonisation %s..."

#: ../src/scanner/main.py:456
#, python-format
msgid ""
"Tuning failed!\n"
"\n"
"Provider: %s\n"
"Tuner: %s\n"
"Frequency: %d MHz\n"
"\n"
"Please check affected tuner for:\n"
"\n"
"Tuner configuration errors,\n"
"Signal cabling issues,\n"
"Any other reception issues."
msgstr ""

#: ../src/scanner/main.py:462
#, python-format
msgid ""
"Tuning lock timed out!\n"
"\n"
"Provider: %s\n"
"Tuner: %s\n"
"Frequency: %d MHz\n"
"\n"
"Please check affected tuner for:\n"
"\n"
"Tuner configuration errors,\n"
"Signal cabling issues,\n"
"Any other reception issues."
msgstr ""

#: ../src/updateproviders.py:196
#, python-format
msgid "Unable to download config file for %s"
msgstr "Impossible de télécharger le fichier de configuration pour %s"

#: ../src/menu.py:87
#, fuzzy
msgid "Update provider files"
msgstr "MiseàjourFournisseurs"

#: ../src/updateproviders.py:285
msgid "Update summary"
msgstr ""

#: ../src/updateproviders.py:49 ../src/updateproviders.py:89
msgid "UpdateProviders"
msgstr "MiseàjourFournisseurs"

#: ../src/setup.py:578
msgid "Wednesday"
msgstr ""

#: ../src/setup.py:509
msgid "When a search finds extra channels that do not have an allocated channel number, 'yes' will add these at the end of the channel list, and 'no' means these will not be included."
msgstr "Lorsqu'une recherche trouve des chaînes supplémentaires qui n'ont pas de numéro de chaîne attribué, 'oui' les ajoutera à la fin de la liste des chaînes, et 'non' signifie qu'elles ne seront pas incluses."

#: ../src/setup.py:501
msgid "When disabled this will enable the 'Keep bouquets' option in the main menu, allowing you to hide some 'existing' bouquets."
msgstr "Quand elle est désactivée, cela active l'option 'garder bouquets' dans le menu principal, permettant de cacher des bouquets 'existants'."

#: ../src/setup.py:512
msgid "When enabled, allows you start a scan from the extensions list."
msgstr "Lorsque cette option est activée, elle vous permet de lancer une analyse à partir de la liste des extensions."

#: ../src/plugin.py:71
#, fuzzy
msgid "bottom"
msgstr "Dessous"

#: ../src/plugin.py:17
msgid "expert"
msgstr "expert"

#: ../src/plugin.py:42 ../src/plugin.py:58
msgid "indent + text"
msgstr "indentation + texte"

#: ../src/setup.py:235 ../src/setup.py:244
msgid "no"
msgstr "non"

#: ../src/plugin.py:41 ../src/plugin.py:57
msgid "none"
msgstr ""

#: ../src/plugin.py:17
msgid "simple"
msgstr "simple"

#: ../src/plugin.py:71
#, fuzzy
msgid "top"
msgstr "Dessus"

#: ../src/setup.py:244
msgid "yes"
msgstr "oui"

#: ../src/setup.py:200
msgid "yes (all channels)"
msgstr "oui (tous les canaux)"

#: ../src/setup.py:222
msgid "yes (custom)"
msgstr "oui (personalisé)"

#: ../src/setup.py:203
msgid "yes (only FTA HD)"
msgstr "oui (seulement HD en clair)"

#: ../src/setup.py:202
msgid "yes (only HD)"
msgstr "oui (seulement HD)"
